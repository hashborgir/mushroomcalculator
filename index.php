<?php

header('Content-Type: application/json');
error_reporting(E_ALL);
ini_set('display_errors', 1);

class Mushroom {

	public $microdose = 0.25;
	public $regular = 2.5;
	public $heroic = 5.0;
	public $wetRatio = 9.3;
	public $wet = null;
	public $maoRatio = 2;
	public $mao = null;
	public $dosageFactor = null;
	public $strains = [
		"azurenscens" => "1.78",
		"bohemica" => "1.34",
		"semilanceata" => ".98",
		"baeocystis" => ".85",
		"cyanescens" => ".85",
		"tampanensis" => ".68",
		"cubensis" => ".63",
		"weilii" => ".61",
		"hoogshagenii" => ".60",
		"stuntzii" => ".36",
		"cyanofibrillosa" => ".21",
		"liniformans" => ".16",
		"penis envy" => "1.35"
	];

	public function __construct() {
		ksort($this->strains, SORT_NATURAL | SORT_STRING);

		$this->wet = $_GET['wet'] ?? 0;
		$this->mao = $_GET['mao'] ?? 0;

		if ($this->wet && $this->mao) {
			$this->microdose = (($this->microdose * $this->wetRatio) / $this->maoRatio);
			$this->regular = (($this->regular * $this->wetRatio) / $this->maoRatio);
			$this->heroic = (($this->heroic * $this->wetRatio) / $this->maoRatio);
		} else
		if ($this->wet) {
			$this->microdose = $this->microdose * $this->wetRatio;
			$this->regular = $this->regular * $this->wetRatio;
			$this->heroic = $this->heroic * $this->wetRatio;
		} else
		if ($this->mao) {
			$this->microdose = $this->microdose / $this->maoRatio;
			$this->regular = $this->regular / $this->maoRatio;
			$this->heroic = $this->heroic / $this->maoRatio;
		}
	}

	public function getAll() {
		$dose = [];
		foreach ($this->strains as $strain => $potency) {
			$dosageFactor = $potency / $this->strains['cubensis'];
			$dose[$strain]['name'] = "Psilocybin " . ucfirst($strain);
			$dose[$strain]['microdose'] = round(($this->microdose / $dosageFactor), 2);
			$dose[$strain]['regular'] = round(($this->regular / $dosageFactor), 2);
			$dose[$strain]['heroic'] = round(($this->heroic / $dosageFactor), 2);
		}
		return json_encode($dose);
	}

	public function getMicrodose() {
		$dose = [];
		foreach ($this->strains as $strain => $potency) {
			$dosageFactor = $potency / $this->strains['cubensis'];
			$dose[$strain]['name'] = "Psilocybin " . ucfirst($strain);
			$dose[$strain]['microdose'] = round(($this->microdose / $dosageFactor), 2);
		}
		return json_encode($dose);
	}

	public function getRegular() {
		$dose = [];
		foreach ($this->strains as $strain => $potency) {
			$dosageFactor = $potency / $this->strains['cubensis'];
			$dose[$strain]['name'] = "Psilocybin " . ucfirst($strain);
			$dose[$strain]['regular'] = round(($this->microdose / $dosageFactor), 2);
		}
		return json_encode($dose);
	}

	public function getHeroic() {
		$dose = [];
		foreach ($this->strains as $strain => $potency) {
			$dosageFactor = $potency / $this->strains['cubensis'];
			$dose[$strain]['name'] = "Psilocybin " . ucfirst($strain);
			$dose[$strain]['heroic'] = round(($this->microdose / $dosageFactor), 2);
		}
		return json_encode($dose);
	}

}

$mushroom = new Mushroom();
echo $mushroom->getAll();
